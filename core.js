// TODO:
//
// 1. Ограничивать input.value только по нижней границе.

// helper

let sortable = [];
let tmpArrayTry04 = [];

function range(a, i, n) {
  let from = Math.max(i + 1 - a[i], 1);
  let to = Math.min(i + 1 + a[i], n);
  tmpArrayTry04[i] = [from - 1, to - 1];
}
function grounds(input) {
  const n = input.length;

  input.forEach((element, i, input) => {
    range(input, i, n);
  });

  return { tmpArray: tmpArrayTry04 };
}

function findResArrayNew(array, inputN, startIndex, resArrayNew, endLenght) {
  let maxIndex = Math.min(startIndex + endLenght + 1, inputN);
  let tmpIndex = startIndex == 0 ? 0 : Number(resArrayNew[resArrayNew.length - 1]) + 1;
  for (let index = tmpIndex; index < maxIndex; index++) {
    const element = array[index];

    // тут будет снова пересчитывать длинну относительно того де сейчас
    // находимся
    // будем брать range[i],делать range[i][0] = startindex, затем
    // пересчитывать длинну, класть в длинну и потом сортировать
    // проще всего пока что переписать что именно кладем в sartable
    if (element[0] <= startIndex) {
      sortable.push([index, element[1] - startIndex]);
    }
  }

  sortable.sort(function(a, b) {
    return a[1] - b[1];
  });

  let longestEscape = sortable[sortable.length - 1][0].toString();

  for (let i = 0, l = array[Number(longestEscape)][1] - startIndex + 1; i < l; i++) {
    resArrayNew.push(longestEscape);
  }

  sortable.length = 0;
}

function f(a) {
  let inputArray = [];
  for (let index = 0; index < a.length; index++) {
    inputArray[index] = Number(a[index]);
  }
  const inputLength = a.length;
  const { tmpArray } = grounds(inputArray);
  let resArrayNew = [];
  let endLenght = Math.min(inputLength, 100);

  for (let j = 0; j < inputLength; null) {
    let startIndex = j;

    findResArrayNew(tmpArray, inputLength, startIndex, resArrayNew, endLenght);
    j = resArrayNew.length;
  }

  const resNew = new Set(resArrayNew).size;
  return resNew;
}

// core
function init() {
  // init model
  let n = 11;
  const model = Array(n)
    .fill(0)
    .map(() => new Array(n).fill(''));

  // render inputs
  let containerInputs = document.querySelector('.container-inputs');
  for (let index = 0; index <= n - 1; index++) {
    let div = document.createElement('div');
    div.setAttribute('class', 'input-block');

    let buttonPlus = document.createElement('button');
    buttonPlus.setAttribute('class', 'js-input-plus');
    buttonPlus.innerHTML = '+';

    let input = document.createElement('input');
    input.setAttribute('type', 'text');
    input.setAttribute('value', 0);
    input.setAttribute('disabled', true);
    input.setAttribute('id', `input-${index}`);

    let buttonMinus = document.createElement('button');
    buttonMinus.setAttribute('class', 'js-input-minus');
    buttonMinus.innerHTML = '-';

    div.appendChild(buttonPlus);
    div.appendChild(input);
    div.appendChild(buttonMinus);
    containerInputs.appendChild(div);
  }

  // render table
  function renderModelToTable(model) {
    let helpTableElement = document.createElement('table');
    model.forEach((el, index) => {
      let tr = document.createElement('tr');
      tr.setAttribute('class', index);

      el.forEach((el2, index2) => {
        let td = document.createElement('td');
        td.setAttribute('class', `${index}.${index2}`);
        td.innerHTML = el2;
        tr.appendChild(td);
      });

      helpTableElement.appendChild(tr);
    });
    document.querySelector('.container-helptable').innerHTML = '';
    document.querySelector('.container-helptable').appendChild(helpTableElement);
  }

  renderModelToTable(model);
  document.querySelectorAll('input').forEach(e => handleInputChange(e));

  // add input buttons behaviour
  document.querySelectorAll('.container-inputs button').forEach(e => {
    e.addEventListener('click', e => {
      let op = e.target.innerHTML;
      let inputElement = e.target.parentElement.querySelector('input');

      switch (op) {
        case '+':
          if (inputElement.value <= 9) {
            inputElement.value++;
            handleInputChange(inputElement);
          }
          break;
        case '-':
          if (inputElement.value >= 1) {
            inputElement.value--;
            handleInputChange(inputElement);
          }
          break;
        default:
          break;
      }
    });
  });

  function renderResult() {
    let inputArray = [];
    document.querySelectorAll('input').forEach(e => {
      inputArray.push(e.value);
    });
    let url =
      'https://activate-fountain-helper.azurewebsites.net/api/HttpTrigger?code=MyxRMphcVfhMYeEFh6gVDyLkhZsMIayJj1w2gW9onGiurj4%2FmDgsMQ%3D%3D';
    let query = inputArray.join('-');
    let headers = {
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': 'GET, POST, OPTIONS',
    };

    fetch(`${url}&name=${query}`, {
      // mode: 'no-cors',
    })
      .then(r => r.json())
      .then(data => {
        console.log(data);
        document.querySelector('.container-result').innerHTML = data;
      });

    // console.log(`inputArray:${inputArray} VS query:${query}`);
    // document.querySelector('.container-result').innerHTML = f(inputArray);
  }

  // add input onChange behaviour by own function
  function handleInputChange(input) {
    // changeModel();
    let indexInModel = Number(input.id.split('-')[1]);
    let value = Number(input.value);

    model[indexInModel] = model[indexInModel].map(() => '');
    // renderModelToTable(model);

    for (let index = Math.max(0, indexInModel - value); index <= Math.min(n - 1, indexInModel + value); index++) {
      // console.log(indexInModel + value);
      model[indexInModel][index] = indexInModel;
    }

    renderModelToTable(model);
    renderResult();
  }
}

window.addEventListener('DOMContentLoaded', init);
